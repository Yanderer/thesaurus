
/// <summary>
// A simple thesaurus ///
// </summary>
public interface IThesaurus
{
    /// <summary>
    /// Adds the given words as synonyms to each other
    /// </summary>
    void addSynonyms(Iterable<String> synonyms);

    /// <summary>
    /// Gets the synonyms for a word
    /// </summary>
    Iterable<String> getSynonyms(String word);

    /// <summary>
    /// Gets all words that are stored in the thesaurus
    /// </summary>
    Iterable<String> getWords();
}