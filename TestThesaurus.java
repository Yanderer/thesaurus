import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestThesaurus {

    public static void main(String[] args) {
        testJoiningSynonyms();
        testSynonymsWithThreads();
    }

    private static void testJoiningSynonyms() {
        System.out.println("testJoiningSynonyms()...");
        IThesaurus thesaurus = new Thesaurus();
        // As result we must get the same list of the synonyms for each
        // word - all of them are synonyms: [1, 2, 3, 4, 5, 6, 7, 8]
        List<String> synonyms = new ArrayList<>();
        synonyms.add("1");
        synonyms.add("2");
        synonyms.add("3");
        thesaurus.addSynonyms(synonyms);

        synonyms = new ArrayList<>();
        synonyms.add("4");
        synonyms.add("5");
        synonyms.add("6");
        thesaurus.addSynonyms(synonyms);

        synonyms = new ArrayList<>();
        synonyms.add("7");
        synonyms.add("2");
        synonyms.add("5");
        synonyms.add("8");
        thesaurus.addSynonyms(synonyms);

        System.out.println("Synonyms for 2 : " + thesaurus.getSynonyms("2"));
        System.out.println("Synonyms for 5 : " + thesaurus.getSynonyms("5"));
        System.out.println("Synonyms for 7 : " + thesaurus.getSynonyms("7"));
        System.out.println("ALL WORDS : " + thesaurus.getWords());
        System.out.println("...testJoiningSynonyms()");
    }

    private static void testSynonymsWithThreads() {
        System.out.println("testSynonymsWithThreads()...");
        ExecutorService executor = Executors.newFixedThreadPool(PARALLEL_THREADS_AMOUNT);

        for (int i = 0; i < TOTAL_TASKS_AMOUNT / 3; ++i) {
            executor.submit(new Writer());
            executor.submit(new Reader());
            executor.submit(new AllReader());
        }

        try {
            sCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();
        System.out.println("...testSynonymsWithThreads()");
    }

    private static final int PARALLEL_THREADS_AMOUNT = 10;

    private static final int TOTAL_TASKS_AMOUNT = 300;

    private static final int MAX_GENERATED_INT = 100;

    private static final int MAX_RANGE = 100;

    private static Random sRandom = new Random(System.currentTimeMillis());

    private static CountDownLatch sCountDownLatch = new CountDownLatch(TOTAL_TASKS_AMOUNT);

    private static IThesaurus sThesaurus = new Thesaurus();

    private static class Writer implements Runnable {

        @Override
        public void run() {
            List<String> synonyms = new ArrayList<>();

            int start = sRandom.nextInt(MAX_GENERATED_INT);
            for (int i = start; i < start + sRandom.nextInt(MAX_RANGE); ++i) {
                synonyms.add(Integer.toString(i));
            }

            sThesaurus.addSynonyms(synonyms);
//            System.out.println("ADDED SYNONYMS: " + synonyms);
            sCountDownLatch.countDown();
        }
    }

    private static class Reader implements Runnable {
        @Override
        public void run() {
            int start = sRandom.nextInt(MAX_GENERATED_INT + MAX_RANGE);
            sThesaurus.getSynonyms(Integer.toString(start));
//            System.out.println(start + " : " + sThesaurus.getSynonyms(Integer.toString(start)));
            sCountDownLatch.countDown();
        }
    }

    private static class AllReader implements Runnable {

        @Override
        public void run() {
            sThesaurus.getWords();
//            System.out.println("ALL WORDS : " + sThesaurus.getWords());
            sCountDownLatch.countDown();
        }
    }

}
