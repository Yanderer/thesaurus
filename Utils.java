
class Utils {

    /**
     * Returns true if string argument null or empty, false otherwise.
     */
    static boolean isEmptyString(String string) {
        return string == null || string.isEmpty();
    }
}
