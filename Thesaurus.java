import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Thread safe thesaurus, which supports parallel reads.
 */
public class Thesaurus implements IThesaurus {

    private final ReadWriteLock lock = new ReentrantReadWriteLock(true);

    private final Map<String, Set<String>> dictionary = new HashMap<>();

    @Override
    public synchronized void addSynonyms(Iterable<String> synonyms) {
        Set<String> newSynonyms = new HashSet<>();

        lock.writeLock().lock();
        try {
            for (String synonym : synonyms) {
                synonym = synonym.trim();
                if (!Utils.isEmptyString(synonym) && !newSynonyms.contains(synonym)) {
                    if (dictionary.containsKey(synonym)) {
                        Set<String> existingSynonyms = dictionary.get(synonym);
                        existingSynonyms.addAll(newSynonyms);
                        for (String synonymToUpdate : newSynonyms) {
                            dictionary.put(synonymToUpdate, existingSynonyms);
                        }
                        newSynonyms = existingSynonyms;
                    } else {
                        newSynonyms.add(synonym);
                        dictionary.put(synonym, newSynonyms);
                    }
                }
            }
            // NOTE: following output is just for debug purposes when running TestThesaurus.main()
            System.out.println("ADDED SYNONYMS: " + synonyms);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * NOTE:
     * Returns list of synonyms for the provided word excluding that word. For example, for synonyms [1,2,3],
     * function call getSynonyms("2") will return [1,3].
     * If there is only one word which doesn't have any synonyms then empty container is returned.
     * If there is no such word in the thesaurus, then null is returned.
     */
    @Override
    public Iterable<String> getSynonyms(String word) {
        String readyWord = word.trim();

        lock.readLock().lock();
        try {
            if (dictionary.containsKey(readyWord)) {
                Set<String> synonyms = dictionary.get(readyWord);
                synonyms = new HashSet<>(synonyms);
                synonyms.remove(readyWord);
                // NOTE: following output is just for debug purposes when running TestThesaurus.main()
                System.out.println(word + " : " + synonyms);
                return synonyms;
            } else {
                // NOTE: following output is just for debug purposes when running TestThesaurus.main()
                System.out.println(word + " : null");
                return null;
            }
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public Iterable<String> getWords() {
        Set<String> allWords = new HashSet<>();

        lock.readLock().lock();
        try {
            for (Set<String> synonyms : dictionary.values()) {
                allWords.addAll(synonyms);
            }
            // NOTE: following output is just for debug purposes when running TestThesaurus.main()
            System.out.println("ALL WORDS : " + allWords);
        } finally {
            lock.readLock().unlock();
        }
        return allWords;
    }

}
